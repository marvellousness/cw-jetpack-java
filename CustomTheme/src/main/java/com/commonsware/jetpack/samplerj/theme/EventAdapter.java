/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.theme;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

class EventAdapter extends ListAdapter<Event, EventViewHolder> {
  private final LayoutInflater inflater;
  private final long startTime;

  EventAdapter(LayoutInflater inflater, long startTime) {
    super(DIFF_CALLBACK);
    this.inflater = inflater;
    this.startTime = startTime;
  }

  @NonNull
  @Override
  public EventViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                            int viewType) {
    View row = inflater.inflate(R.layout.row, parent, false);

    return new EventViewHolder(row, startTime);
  }

  @Override
  public void onBindViewHolder(@NonNull EventViewHolder holder, int position) {
    holder.bindTo(getItem(position));
  }

  private static final DiffUtil.ItemCallback<Event> DIFF_CALLBACK =
    new DiffUtil.ItemCallback<Event>() {
      @Override
      public boolean areItemsTheSame(@NonNull Event oldEvent, @NonNull Event newEvent) {
        return oldEvent == newEvent;
      }

      @Override
      public boolean areContentsTheSame(@NonNull Event oldEvent, @NonNull Event newEvent) {
        return oldEvent.timestamp == newEvent.timestamp &&
          oldEvent.message.equals(newEvent.message) &&
          oldEvent.activityHash == newEvent.activityHash &&
          oldEvent.viewmodelHash == newEvent.viewmodelHash;
      }
    };
}