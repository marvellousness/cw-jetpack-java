/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.theme;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;

class Event implements Parcelable {
  final long timestamp;
  final String message;
  final int activityHash;
  final int viewmodelHash;

  Event(String message, int activityHash, int viewmodelHash) {
    this.message = message;
    this.activityHash = activityHash;
    this.viewmodelHash = viewmodelHash;
    this.timestamp = SystemClock.elapsedRealtime();
  }

  protected Event(Parcel in) {
    timestamp = in.readLong();
    message = in.readString();
    activityHash = in.readInt();
    viewmodelHash = in.readInt();
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(timestamp);
    dest.writeString(message);
    dest.writeInt(activityHash);
    dest.writeInt(viewmodelHash);
  }

  @SuppressWarnings("unused")
  public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {
    @Override
    public Event createFromParcel(Parcel in) {
      return new Event(in);
    }

    @Override
    public Event[] newArray(int size) {
      return new Event[size];
    }
  };
}